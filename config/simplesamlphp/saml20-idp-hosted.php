<?php

/* The index of the array is the entity ID of this IdP. */
$metadata['https://idp.example.com/simplesaml/saml2/idp/metadata.php'] = array(

    'host' => 'idp.example.com',

    /* Configuration options. */

    /* Organization info */
    'OrganizationName' => array(
        'en' => 'Example organization',
        'es' => 'Organizacion de ejemplo',
    ),
    'OrganizationURL' => array(
        'en' => 'http://idp.example.com',
        'es' => 'http://idp.example.com',
    ),

    /* The private key and certificate used by this IdP. */
    'certificate' => 'idp.example.com.crt',
    'privatekey' => 'idp.example.com.key',

    /*
     * The authentication source for this IdP. Must be one
     * from config/authsources.php.
     */
    'auth' => 'example-userpass',

    // Logout requests and logout responses sent from this IdP should be signed
    'redirect.sign' => TRUE,
    // All communications are encrypted
    'assertion.encryption' => TRUE,
);




/* The index of the array is the entity ID of this IdP. */
$metadata['https://idp.example.com/simplesaml/saml2/idp/metadata.php'] = array(

    'host' => 'idp.example.com',

    /* Configuration options. */

    /* Organization info */
    'OrganizationName' => array(
        'en' => 'Example organization',
        'es' => 'Organizacion de ejemplo',
    ),
    'OrganizationURL' => array(
        'en' => 'http://idp.example.com',
        'es' => 'http://idp.example.com',
    ),

    /* The private key and certificate used by this IdP. */
    'certificate' => 'idp.example.com.crt',
    'privatekey' => 'idp.example.com.key',

    /*
     * The authentication source for this IdP. Must be one
     * from config/authsources.php.
     */
    'auth' => 'example-userpass',

    // Logout requests and logout responses sent from this IdP should be signed
    'redirect.sign' => TRUE,
    // All communications are encrypted
    'assertion.encryption' => TRUE,
);

?>

?>