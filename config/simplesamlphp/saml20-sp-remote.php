<?php
/**
 * SAML 2.0 remote SP metadata for SimpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote
 */

$metadata[getenv('SIMPLESAMLPHP_SP_ENTITY_ID')] = array(
    'AssertionConsumerService' => getenv('SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE'),
    'SingleLogoutService' => getenv('SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE'),
);

$metadata['http://localhost:8080/simplesaml/saml2/idp/metadata.php'] = array(
    'name' => array(
        'en' => 'Test IdP',
    ),
    'description' => 'Test IdP',
    'SingleSignOnService' => 'http://localhost:8080/simplesaml/saml2/idp/SSOService.php',
    'SingleLogoutService' => 'http://localhost:8080/simplesaml/saml2/idp/SingleLogoutService.php',
    'certFingerprint' => '119b9e027959cdb7c662cfd075d9e2ef384e445f',
);



/* The index of the array is the entity ID of this IdP. */
$metadata['https://idp.example.com/simplesaml/saml2/idp/metadata.php'] = array(

    'host' => 'idp.example.com',

    /* Configuration options. */

    /* Organization info */
    'OrganizationName' => array(
        'en' => 'Example organization',
        'es' => 'Organizacion de ejemplo',
    ),
    'OrganizationURL' => array(
        'en' => 'http://idp.example.com',
        'es' => 'http://idp.example.com',
    ),

    /* The private key and certificate used by this IdP. */
    'certificate' => 'idp.example.com.crt',
    'privatekey' => 'idp.example.com.key',

    /*
     * The authentication source for this IdP. Must be one
     * from config/authsources.php.
     */
    'auth' => 'example-userpass',

    // Logout requests and logout responses sent from this IdP should be signed
    'redirect.sign' => TRUE,
    // All communications are encrypted
    'assertion.encryption' => TRUE,
);

?>
